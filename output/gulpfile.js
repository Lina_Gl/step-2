const { task, src, dest, parallel, watch, series } = require("gulp");
const del = require('del')
const csso = require('gulp-csso');
const concat = require('gulp-concat');
const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify-es').default;
const imgMin = require('gulp-imagemin');
const browserSync = require('browser-sync');


//file's paths
const paths = {
    script: 'src/js/*.js',
    css: 'src/css/*.css',
    scss: 'src/scss/**/*.scss',
    img: 'src/img/**/*.{jpg,png,svg}',
    html: '*.html'
}

//from scss to css
function css() {
    return src(paths.scss).pipe(sass()).pipe(dest('src/css/'))
}

//concat css to 1 file, minify add autoprefixes and move to dist folder
function cssAutoPrMin() {
    return src(paths.css).pipe(concat('style.min.css'))
        .pipe(csso())
        .pipe(autoprefixer('last 2 version'))
        .pipe(dest('dist/css/'))
        .pipe(browserSync.reload({
            stream: true
        }))
}

//concat js files to 1 file, minimize and move to dist folder
function jsMin() {
    return src(paths.script)
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(dest('dist/js/'))
        .pipe(browserSync.reload({
            stream: true
        }))
}

//minimize images and move to dist folder
function imageMin() {
    return src(paths.img)
        .pipe(imgMin())
        .pipe(dest('dist/img/'))
}

//coping html
function htmlCopy() {
    return src(paths.html)
        .pipe(dest('dist/'))
        .pipe(browserSync.reload({
            stream: true
        }))
}
//clean dist folder
function clean() {
    return del('dist/')
}
//task to watch js and scss files
task('watch', function() {
        watch(paths.html, htmlCopy);
        watch(paths.script, jsMin)
        watch(paths.scss, series(css, cssAutoPrMin))
    })

// browser synchronize
task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: 'dist/'
        }
    });
});

function streamTask() {
    return src('*.js')
        .pipe(dest('output'));
}

exports.default = streamTask;

task('build', series(clean, css, cssAutoPrMin, jsMin, imageMin, htmlCopy))
task('dev', parallel('watch', streamTask, 'browser-sync'))