$(document).ready(function() {
    $('.navbar__burger').click(function(e) {
        e.preventDefault();
        $('.navbar__list').toggleClass('navbar__list-open');
        $('.navbar__lines').toggleClass('open');
    });
});